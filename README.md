# Api simple pour agregio

1 Controller pour les requêtes pour les offres  
1 Controller pour les requêtes pour les parcs  

2 Entités JPA pour garder en mémoire les données sur les offres et les parcs  
Les parcs peuvent avoir 0,1 ou plusieurs offres  
Une offre appartient à un seul parc  
Un parc peut avoir des offres sur des marchés différents  
Types de marché : PRIMARY, SECONDARY, RAPID  
Types de parcs : SOLAR, WIND, HYDRAULIC  

Exemples d'appel :  
GET http://localhost:8080/farms/PRIMARY Pour lister les parcs présents sur le marché primaire  
POST http://localhost:8080/farms  Pour créer un parc  
Request Body :  
{
"name":"Parc 7",
"type":"WIND"
}  

GET http://localhost:8080/offers/PRIMARY  Pour lister les offres présentes sur le marché primaire     
POST http://localhost:8080/offers  Pour créer une offre  
Request Body :   
{
"quantity":99,
"minimumPrice":100,
"startTime":"2022-02-27T23:10:00",
"endTime":"2022-02-28T07:10:00",
"marketType":"PRIMARY",
"farm":{
"id":1
}
}  
