package com.agregio.demo.web;

import com.agregio.demo.domain.Farm;
import com.agregio.demo.domain.Offer;
import com.agregio.demo.domain.enumeration.FarmType;
import com.agregio.demo.domain.enumeration.MarketType;
import com.agregio.demo.service.FarmService;
import com.agregio.demo.service.OfferService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class OfferControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OfferService offerService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void callPostOffers() throws Exception {
        Offer o = new Offer(null, 100L, 1000L, LocalDateTime.now(), LocalDateTime.now(), MarketType.PRIMARY, new Farm(1L, null, null, null) );
        given(offerService.createOffer(o))
                .willReturn(o);

        mockMvc.perform(post("/offers")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(o))
                        .characterEncoding("utf-8"))
                .andExpect(status().isCreated());

        verify(offerService, times(1)).createOffer(any());
    }

    @Test
    public void callPostOffersException() throws Exception {
        Offer o = new Offer();
        o.setId(1L);
        given(offerService.createOffer(o))
                .willReturn(o);

        mockMvc.perform(post("/offers")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(o))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());

        verify(offerService, times(0)).createOffer(any());
    }


    @Test
    public void callGetOffers() throws Exception {
        List<Offer> list = new ArrayList<>();
        given(offerService.getOffersByMarketType(MarketType.PRIMARY))
                .willReturn(list);

        mockMvc.perform(get("/offers/PRIMARY"))
                .andExpect(status().is2xxSuccessful());

        verify(offerService, times(1)).getOffersByMarketType(MarketType.PRIMARY);
    }


}
