package com.agregio.demo.web;

import com.agregio.demo.domain.Farm;
import com.agregio.demo.domain.enumeration.FarmType;
import com.agregio.demo.domain.enumeration.MarketType;
import com.agregio.demo.service.FarmService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class FarmControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FarmService farmService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void callPostFarms() throws Exception {
        Farm f = new Farm(null, "Parc 1", FarmType.WIND, null);
        given(farmService.createFarm(f))
                .willReturn(f);

        mockMvc.perform(post("/farms")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(f))
                        .characterEncoding("utf-8"))
                .andExpect(status().isCreated());

        verify(farmService, times(1)).createFarm(any());
    }

    @Test
    public void callPostFarmsException() throws Exception {
        Farm f = new Farm();
        f.setId(1L);
        given(farmService.createFarm(f))
                .willReturn(f);

        mockMvc.perform(post("/farms")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(f))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());

        verify(farmService, times(0)).createFarm(any());
    }


    @Test
    public void callGetFarms() throws Exception {
        List<Farm> list = new ArrayList<>();
        given(farmService.getFarmsByMarketType(MarketType.PRIMARY))
                .willReturn(list);

        mockMvc.perform(get("/farms/PRIMARY"))
                .andExpect(status().is2xxSuccessful());

        verify(farmService, times(1)).getFarmsByMarketType(MarketType.PRIMARY);
    }


}
