package com.agregio.demo.service;

import com.agregio.demo.domain.Farm;
import com.agregio.demo.domain.Offer;
import com.agregio.demo.domain.enumeration.MarketType;
import com.agregio.demo.repository.FarmRepository;
import com.agregio.demo.repository.OfferRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@AutoConfigureMockMvc
public class OfferServiceTest {

    @Autowired
    private OfferService offerService;

    @MockBean
    private OfferRepository offerRepository;

    @Test
    public void callCreateOffer() {
        Offer o = new Offer();
        given(offerRepository.save(o))
                .willReturn(o);

        offerService.createOffer(o);

        verify(offerRepository, times(1)).save(o);
    }

    @Test
    public void callGetOffersByMarketType() {
        List<Offer> list = new ArrayList<>();
        given(offerRepository.findOfferByMarketType(MarketType.PRIMARY))
                .willReturn(list);

        offerService.getOffersByMarketType(MarketType.PRIMARY);

        verify(offerRepository, times(1)).findOfferByMarketType(MarketType.PRIMARY);
    }
}
