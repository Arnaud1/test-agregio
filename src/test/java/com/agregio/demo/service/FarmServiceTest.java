package com.agregio.demo.service;

import com.agregio.demo.domain.enumeration.FarmType;
import com.agregio.demo.domain.enumeration.MarketType;
import org.junit.Assert;
import com.agregio.demo.domain.Farm;
import com.agregio.demo.repository.FarmRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@AutoConfigureMockMvc
public class FarmServiceTest {

    @Autowired
    private FarmService farmService;

    @MockBean
    private FarmRepository farmRepository;

    @Test
    public void callCreateFarm() {
        Farm f = new Farm(1L, "Parc 1", FarmType.WIND, null);
        given(farmRepository.save(f))
                .willReturn(f);

        farmService.createFarm(f);

        verify(farmRepository, times(1)).save(f);
    }

    @Test
    public void callGetFarmsByMarketType() {
        List<Farm> list = new ArrayList<>();
        given(farmRepository.findDistinctByOffersMarketType(MarketType.PRIMARY))
                .willReturn(list);

        farmService.getFarmsByMarketType(MarketType.PRIMARY);

        verify(farmRepository, times(1)).findDistinctByOffersMarketType(MarketType.PRIMARY);
    }
}
