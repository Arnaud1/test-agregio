package com.agregio.demo.service.impl;

import com.agregio.demo.domain.Farm;
import com.agregio.demo.domain.enumeration.MarketType;
import com.agregio.demo.repository.FarmRepository;
import com.agregio.demo.service.FarmService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class FarmServiceImpl implements FarmService {

    private final FarmRepository farmRepository;

    @Override
    public Farm createFarm(Farm farm) {
        return farmRepository.save(farm);
    }

    @Override
    public List<Farm> getFarmsByMarketType(MarketType marketType) {
        return farmRepository.findDistinctByOffersMarketType(marketType);
    }
}
