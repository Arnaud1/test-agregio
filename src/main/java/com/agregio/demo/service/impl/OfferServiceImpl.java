package com.agregio.demo.service.impl;

import com.agregio.demo.domain.Offer;
import com.agregio.demo.domain.enumeration.MarketType;
import com.agregio.demo.repository.OfferRepository;
import com.agregio.demo.service.OfferService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class OfferServiceImpl implements OfferService {

    private final OfferRepository offerRepository;

    @Override
    public Offer createOffer(Offer offer) {
        return offerRepository.save(offer);
    }

    @Override
    public List<Offer> getOffersByMarketType(MarketType marketType) {
        return offerRepository.findOfferByMarketType(marketType);
    }
}
