package com.agregio.demo.service;

import com.agregio.demo.domain.Farm;
import com.agregio.demo.domain.enumeration.MarketType;

import java.util.List;

public interface FarmService {

    Farm createFarm(Farm farm);

    List<Farm> getFarmsByMarketType(MarketType marketType);
}
