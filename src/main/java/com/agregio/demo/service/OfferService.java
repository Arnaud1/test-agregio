package com.agregio.demo.service;

import com.agregio.demo.domain.Offer;
import com.agregio.demo.domain.enumeration.MarketType;

import java.util.List;

public interface OfferService {

    Offer createOffer(Offer offer);

    List<Offer> getOffersByMarketType(MarketType marketType);
}
