package com.agregio.demo.web.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller advice to catch thrown exceptions
 */
@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler
    @ResponseBody
    public JsonError handleOfferException(HttpServletRequest req, CustomException e){
        return new JsonError(req.getRequestURL().toString(), e.getMessage());
    }
}
