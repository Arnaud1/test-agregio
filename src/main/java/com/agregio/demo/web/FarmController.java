package com.agregio.demo.web;

import com.agregio.demo.domain.Farm;
import com.agregio.demo.domain.enumeration.MarketType;
import com.agregio.demo.service.FarmService;
import com.agregio.demo.web.exceptions.CustomException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller for Farm Resource
 */
@RestController
@AllArgsConstructor
public class FarmController {

    private final FarmService farmService;

    /**
     *
     * @param marketType
     * @return the list of farms with offer on given market
     */
    @GetMapping("/farms/{market}")
    public List<Farm> getFarmsOfGivenMarket(@PathVariable(value = "market") MarketType marketType){
        return farmService.getFarmsByMarketType(marketType);
    }

    /**
     * Create farm if it does not exist yet
     * @param farm to create
     * @return the created farm
     * or error if the farm is not properly set
     */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/farms")
    public Farm createFarm(@Valid @RequestBody Farm farm){
        if(farm.getId() != null)
            throw new CustomException(HttpStatus.BAD_REQUEST, "id must be null");
        Farm result = this.farmService.createFarm(farm);
        return result;
    }
}
