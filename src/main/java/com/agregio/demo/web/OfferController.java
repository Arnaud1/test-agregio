package com.agregio.demo.web;

import com.agregio.demo.domain.Offer;
import com.agregio.demo.domain.enumeration.MarketType;
import com.agregio.demo.service.OfferService;
import com.agregio.demo.web.exceptions.CustomException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller for Offer Resource
 */
@RestController
@AllArgsConstructor
public class OfferController {

    private final OfferService offerService;

    /**
     *
     * @param marketType
     * @return the list of offers available on a given market type
     */
    @GetMapping("/offers/{marketType}")
    public List<Offer> getFarmsOfGivenMarket(@PathVariable(value = "marketType") MarketType marketType){
        return offerService.getOffersByMarketType(marketType);
    }

    /**
     * Create offer
     * @param offer to create
     * @return the created offer
     * Or error if the offer is not properly set
     */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/offers")
    public Offer createOffer(@Valid @RequestBody Offer offer){
        if(offer.getId() != null)
            throw new CustomException(HttpStatus.BAD_REQUEST, "id must be null");
        if(offer.getFarm() == null)
            throw new CustomException(HttpStatus.BAD_REQUEST, "an offer must be linked to a farm");
        if(offer.getEndTime().isBefore(offer.getStartTime()))
            throw new CustomException(HttpStatus.BAD_REQUEST, "Start date must be inferior to end date");
        Offer result = this.offerService.createOffer(offer);
        return result;
    }

}
