package com.agregio.demo.repository;

import com.agregio.demo.domain.Offer;
import com.agregio.demo.domain.enumeration.MarketType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Long> {
    List<Offer> findOfferByMarketType(MarketType typeMarket);
}
