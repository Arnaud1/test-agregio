package com.agregio.demo.repository;

import com.agregio.demo.domain.Farm;
import com.agregio.demo.domain.enumeration.MarketType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FarmRepository extends JpaRepository<Farm, Long> {
    List<Farm> findDistinctByOffersMarketType(MarketType marketType);
}
