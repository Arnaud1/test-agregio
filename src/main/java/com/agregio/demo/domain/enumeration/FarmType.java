package com.agregio.demo.domain.enumeration;

public enum FarmType {
    WIND,
    SOLAR,
    HYDRAULIC
}
