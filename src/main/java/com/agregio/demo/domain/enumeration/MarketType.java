package com.agregio.demo.domain.enumeration;

public enum MarketType {
    PRIMARY,
    SECONDARY,
    RAPID
}
