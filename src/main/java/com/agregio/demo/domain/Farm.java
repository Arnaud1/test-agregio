package com.agregio.demo.domain;

import com.agregio.demo.domain.enumeration.FarmType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Table(name = "farm")
public class Farm implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotNull(message = "a farm name must be given")
    @Column(name = "farm_name")
    private String name;


    @Enumerated(EnumType.STRING)
    @Column(name = "farm_type")
    private FarmType type;

    @OneToMany(mappedBy = "farm")
    @JsonIgnore
    private Set<Offer> offers;
}