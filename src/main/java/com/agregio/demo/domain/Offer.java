package com.agregio.demo.domain;

import com.agregio.demo.domain.enumeration.MarketType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Table(name = "offer")
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotNull(message = "Quantity must be set")
    @Min(1)
    @Column(name = "quantity")
    private Long quantity;

    @NotNull(message = "Minimum price must be set")
    @Min(1)
    @Column(name = "minimum_price")
    private Long minimumPrice;

    @NotNull(message = "Start time must be set")
    @Column(name = "start_time")
    private LocalDateTime startTime;

    @NotNull(message = "End time must be set")
    @Column(name = "end_time")
    private LocalDateTime endTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "market_type")
    private MarketType marketType;


    @NotNull(message = "a farm must be associated")
    @ManyToOne
    @JoinColumn(name = "farm_id")
    private Farm farm;
}