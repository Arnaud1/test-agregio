INSERT
INTO
  farm
  (id, farm_name, farm_type)
VALUES
  (NEXTVAL('hibernate_sequence'), 'Parc 1', 'WIND');
INSERT
INTO
  farm
  (id, farm_name, farm_type)
VALUES
  (NEXTVAL('hibernate_sequence'), 'Parc 2', 'SOLAR');
INSERT
INTO
  farm
  (id, farm_name, farm_type)
VALUES
  (NEXTVAL('hibernate_sequence'), 'Parc 3', 'HYDRAULIC');

INSERT
INTO
  offer
  (id, quantity, minimum_price, start_time, end_time, market_type, farm_id)
VALUES
  (NEXTVAL('hibernate_sequence'), 1000, 10, NOW(), NOW()+1, 'PRIMARY', 1);
INSERT
INTO
  offer
  (id, quantity, minimum_price, start_time, end_time, market_type, farm_id)
VALUES
  (NEXTVAL('hibernate_sequence'), 1000, 10, NOW(), NOW()+1, 'SECONDARY', 2);
INSERT
INTO
  offer
  (id, quantity, minimum_price, start_time, end_time, market_type, farm_id)
VALUES
  (NEXTVAL('hibernate_sequence'), 1000, 10, NOW(), NOW()+1, 'RAPID', 1);
INSERT
INTO
  offer
  (id, quantity, minimum_price, start_time, end_time, market_type, farm_id)
VALUES
  (NEXTVAL('hibernate_sequence'), 1000, 10, NOW(), NOW()+1, 'PRIMARY', 3);